package com.example.ecoleapandroid.tools.recipe;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecoleapandroid.R;
import com.example.ecoleapandroid.models.Recipe;
import com.example.ecoleapandroid.ui.recipes.CompleteRecipe;

import org.jetbrains.annotations.NotNull;

public class RecipeViewHolder extends RecyclerView.ViewHolder {
    //private ImageView recipeIcon;
    private TextView recipeTitle;
    private String recipeTitleText;
    private String recipeIngredient;
    private String recipeText;

    public RecipeViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);
        //recipeIcon = itemView.findViewById(R.id.imageViewRecipeIcon);
        recipeTitle = itemView.findViewById(R.id.textViewRecipeTitle);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CompleteRecipe.class);
                intent.putExtra("title",recipeTitleText);
                intent.putExtra("ingredient",recipeIngredient);
                intent.putExtra("text",recipeText);
                view.getContext().startActivity(intent);
            }
        });
    }

    public void display(Recipe recipe){
        //Bitmap bm = BitmapFactory.decodeResource(this.itemView.getResources(), R.drawable.ic_recipe_test_foreground);
        //recipeIcon.setImageBitmap(bm);
        recipeTitleText = recipe.getTitle();
        recipeTitle.setText(recipeTitleText);
        recipeIngredient = recipe.getIngredient();
        recipeText = recipe.getText();
    }
}
