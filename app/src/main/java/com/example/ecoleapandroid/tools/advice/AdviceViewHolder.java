package com.example.ecoleapandroid.tools.advice;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecoleapandroid.R;
import com.example.ecoleapandroid.models.Advice;
import com.example.ecoleapandroid.ui.advices.CompleteAdviceActivity;

import org.jetbrains.annotations.NotNull;

public class AdviceViewHolder extends RecyclerView.ViewHolder {
    private ImageView adviceIcon;
    private TextView adviceTitle;
    private Advice advice;
    private Context context;

    public AdviceViewHolder(@NonNull @NotNull View itemView) {
        super(itemView);
        adviceIcon = itemView.findViewById(R.id.imageViewAdviceIcon);
        adviceTitle = itemView.findViewById(R.id.textViewAdviceTitle);
        context = itemView.getContext();

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), CompleteAdviceActivity.class);
                Advice a = getAdviceInstance();

                intent.putExtra("advice_title", adviceTitle.getText());
                intent.putExtra("advice_description", a.getText());
                intent.putExtra("advice_icon", a.getIcon());

                view.getContext().startActivity(intent);
            }
        });
    }

    public Advice getAdviceInstance(){
        return advice;
    }

    public void setAdviceInstance(Advice a){
        this.advice = a;
    }

    public void display(Advice advice){
        int iconRessource = context.getResources().getIdentifier(advice.getIcon(), "drawable", context.getPackageName());
        if (iconRessource == 0){
            iconRessource = R.drawable.ic_leaf_foreground;
        }

        adviceIcon.setImageResource(iconRessource);
        adviceTitle.setText(advice.getTitle());
    }
}
