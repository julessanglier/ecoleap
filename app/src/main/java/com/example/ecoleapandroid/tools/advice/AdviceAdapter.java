package com.example.ecoleapandroid.tools.advice;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecoleapandroid.R;
import com.example.ecoleapandroid.models.Advice;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class AdviceAdapter extends RecyclerView.Adapter<AdviceViewHolder>  {
    private static List<Advice> advices;

    public AdviceAdapter(List<Advice> a) {
        advices = a;
    }

    @NonNull
    @NotNull
    @Override
    public AdviceViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.advice_cell, parent, false);
        AdviceViewHolder holder = new AdviceViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AdviceViewHolder holder, int position) {
        Advice a = advices.get(position);
        holder.display(a);
        holder.setAdviceInstance(a);
    }

    @Override
    public int getItemCount() {
        return advices.size();
    }
}
