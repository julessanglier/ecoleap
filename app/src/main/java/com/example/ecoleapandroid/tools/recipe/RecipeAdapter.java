package com.example.ecoleapandroid.tools.recipe;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecoleapandroid.R;
import com.example.ecoleapandroid.models.Recipe;
import com.example.ecoleapandroid.tools.recipe.RecipeViewHolder;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class RecipeAdapter extends RecyclerView.Adapter<RecipeViewHolder>  {
    private static List<Recipe> recipes;

    public RecipeAdapter(List<Recipe> a) {
        recipes = a;
    }

    @NonNull
    @NotNull
    @Override
    public RecipeViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recipe_cell, parent, false);
        RecipeViewHolder holder = new RecipeViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecipeViewHolder holder, int position) {
        Recipe r = recipes.get(position);
        holder.display(r);
    }

    @Override
    public int getItemCount() {
        return recipes.size();
    }
}
