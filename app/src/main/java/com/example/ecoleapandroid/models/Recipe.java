package com.example.ecoleapandroid.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "recipe")
public class Recipe {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name="title")
    private String title;

    @ColumnInfo(name="ingredient")
    private String ingredient;

    @ColumnInfo(name="text")
    private String text;


    public Recipe(String title, String ingredient, String text) {
        this.title = title;
        this.ingredient = ingredient;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int idRecipe) {
        this.id = idRecipe;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static Recipe[] populateRecipes(){
        return new Recipe[]{
                new Recipe("Tarte aux pesto", "- 2 Pâtes feuilletées \n-Pesto Rouge ou vert selon les préférences", "Prendre 1 pâtes feuilletées. Faire des petits trous avec une fourchette dans la pâte, puis étalé à votre convenance le pesto de votre choix. Apposez le deuxième ronds de pâtes sur le premier. Couper la tarte en soleil, puis torsadez la pate. Mettre au four à 180°C pendant 25min."),
                new Recipe("Houmous fanes de radis", "-250g radis \n-250g de pois chiches cuits \n-2 cas tahnin \n-6cl d'huile d'olive","Laver les radis et les fanes, les séchez. Mettre les fanes et le reste des inrédients sauf l'huie dans un robot, mixer puis ajouter l'huile. Saler puis servir le houmous avec les radis"),
                new Recipe("Glace aux framboises","- 1 banane \n-150g de frambroises \n-10cl de lait d'amande non sucré \n 1 càs sirop d'agave", "Mettre tous les ingrédients dans un blender. Mixer jusqu'à consistance lisse. Mettre dans les moules, ajouter les bâtonnets et placer au congélateur pour 4 heures"),
                new Recipe("Brochettes tomates séchés avocat et olives noires", "-4 tomates séchées \n-1 avocat \n-20g olives noires dénoyautées", "Egoutter les tomates sechees et les couper en 2 ou 2. Couper les avocats en morceaux. Monter les brochettes en alternant les morceaux differents a votre convenance."),
                new Recipe("Salade de riz","-1 boite de thon \n-du riz \n-une boite de mais \n-8 bâtonnets de surimi","Prendre les ingrédients les mélanger dans un saladier. Puis mettre de la vinaigrette"),
                new Recipe("Chou fleur froid", "-1 Chou Fleur \n-Vinaigrette Balsamique", "Faire cuire le chou fleur, le laissez refroidir. Le servir avec de la vinaigrette"),
                new Recipe("Pancake a la banane", "-1 banane \n-2 oeufs", "Battre les oeufs, écraser la banane, mélanger les deux. Puis faites cuires dans une poele bien chaude"),
                new Recipe("Milkshale à la banane", "-1 banane \n-2 glaçons \n-1/2 litre de lait \n-1 sachet de sucre vanillé","Mettre le tout dans le blender, mixer puis servir frais"),
        };
    }

}
