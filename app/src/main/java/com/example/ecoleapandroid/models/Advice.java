package com.example.ecoleapandroid.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "advice")
public class Advice {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name="title")
    private String title;

    @ColumnInfo(name="icon")
    private String icon;

    @ColumnInfo(name="text")
    private String text;

    public Advice(String title, String icon, String text) {
        this.title = title;
        this.icon = icon;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static Advice[] populateData(){
        return new Advice[]{
                new Advice("Avoir une gourde", "", "L'association de consommateurs UFC-Que Choisir rappelle que l'eau vendue en bouteille génère 10 à 20 millions de m³ de déchets par an. Sauf contre-indication, l'eau du robinet est tout aussi potable. Privilégiez donc les carafes à table et pour la journée achetez une gourde réutilisable, qui représente une alternative aux multiples gobelets à usage unique utilisés à la fontaine. Pour la pause café, il suffit d'apporter sa bouteille isotherme ou sa propre tasse. Chaque seconde, 150 gobelets sont jetés en France."),
                new Advice("Acheter en vrac", "", "Selon l'association Zero Waste France, 25,8 millions de tonnes de déchets plastiques sont produites chaque année en Europe, dont 60 % proviennent des emballages. Préférez donc les fruits et légumes non-emballés. Plus largement, tous les produits doivent être passés au crible. Au rayon hygiène ou ménage, par exemple, les éco-recharges permettent de réduire considérablement sa consommation d'emballages. \n" +
                        "\n" +
                        "La solution peut aussi résider dans l'achat en vrac, dont l'offre ne cesse de se développer. Le nombre d'épiceries qui proposent ce mode de consommation est passé de 18 en 2015 à 160 en 2018. Et les grandes surfaces ont franchi le cap. Café, graines, pâtes, riz... De plus en plus de produits sont disponibles. L'achat en vrac permet également d'éviter le gaspillage alimentaire, puisque les clients choisissent eux-mêmes la quantité qu'ils estiment nécessaire. "),
                new Advice("Composter ses déchêts organiques", "", "Le compostage permet de produire un compost 100% naturel et gratuit, d'enrichir la terre de son jardin en matière organique... \n\nEn moyenne chaque foyer peut composter 100kg de déchets par an. Le volume de la poubelle se réduit donc et en plus, plus besoin d'amener à la déchèterie les déchets de jardin qui peuvent se composter avec les restes alimentaires. "),
                new Advice("Trier ses déchêts", "", "Les problèmes qui menacent notre environnement aujourd’hui vous préoccupe. Vous voulez faire un geste simple, quotidien et durable : pratiquez le tri sélectif.\n" +
                        "\n" +
                        "Tout commence par le traitement de vos ordures ménagères.\n" +
                        "\n" +
                        "Par an, chaque français est producteur de près de 360 kg de déchets. Depuis une vingtaine d’année ce nombre n’a cessé de croître et à doubler.\n" +
                        "\n" +
                        "Généralement, les ordures sont mises en décharge, incinérées et parfois compostées. De l’énergie et des ressources gaspillées car certains matériaux tels que le verre, le métal, le papier ou encore le carton lorsqu’ils sont triés peuvent être réutilisés et ainsi avoir une seconde vie.\n" +
                        "\n" +
                        "D’où tout l’intérêt du tri selectif."),
                new Advice("Prendre les transports en commun", "", "Voyager en transport en commun présente de nombreux bienfaits tels que l’amélioration du trafic, et réduit la congestion en ville, tout en permettant d’émettre le moins de substances polluantes dans l’air. L’autre avantage est la baisse de pollution sonore, et un meilleur vivre-ensemble, tout en économisant de l’argent. Selon une étude menée aux États-Unis, une famille de classe moyenne qui prend les transports en commun économise environ 5000 euros sur ses dépenses courantes, de quoi faire réfléchir. Moins de voitures sur les routes permet un environnement plus propre et plus sain pour tous."),
                new Advice("Régime végétarien en pratique", "", "Les avantages du végétarisme sur la santé qui ont été démontrés scientifiquement sont les suivants : moins de problèmes d’obésité, moins de maladies cardiovasculaires, moins d’hypertension artérielle, moins de diabète de type 2, moins de cancers, moins de troubles intellectuels chez le sujet âgé. Quand on sait que ces problèmes de santé sont de véritables fléaux dans notre société, et constituent la majorité des décès, ça donne envie de devenir végétarien non ?"),
                new Advice("La mode éthique et écologique", "", "les avantages de la mode éthique se définissent par un respect de l’environnement, avec une utilisation de matières biologiques (tel le coton biologique, le chanvre ou le lin) qui ne mettent en aucun cas en danger la faune et la flore de notre planète ainsi que les Hommes. Elle préserve ainsi nos ressources naturelles. De plus, il y a un mode de fabrication lent avec une production limitée aussi appelé “slow-fashion” qui réduit le gaspillage, les déchets et la surproduction. Ce processus améliore les conditions de travail des employés et favorise le respect des conditions de travail de chaque individu et leur permet d’avoir un salaire décent avec  des heures de travail réglementaires, c'est tout bénéf non ?")
        };
    }
}
