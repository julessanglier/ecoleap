package com.example.ecoleapandroid.models;

import android.content.Context;
import android.util.Log;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.example.ecoleapandroid.utils.FileUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@Entity(tableName = "place")
public class Place {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name="type")
    private String type;

    @ColumnInfo(name="name")
    private String name;

    @ColumnInfo(name="description")
    private String description;

    @ColumnInfo(name="lat")
    private float latitude;

    @ColumnInfo(name="long")
    private float longitude;

    public Place(String type, String name, String description, float latitude, float longitude) {
        //this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latatitude) {
        this.latitude = latatitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString(){
        return this.name;
    }

    public static Place[] populateMagasins(Context context) throws JSONException {
        String raw_json = FileUtils.getJsonFromAssets(context, "magasins_zero_dechet.geojson");
        JSONObject obj = new JSONObject(raw_json);
        JSONArray featuresArray = obj.getJSONArray("features");
        List<Place> outputPlaces = new ArrayList<>();

        for (int i = 0; i < featuresArray.length(); i++){
            JSONObject featureArray = featuresArray.getJSONObject(i);
            JSONObject propertiesArray = featureArray.getJSONObject("properties");
            JSONObject geometryArray = featureArray.getJSONObject("geometry");
            String name = "";
            String desc = "";
            double latitude = 0;
            double longitude = 0;

            name = propertiesArray.getString("name");
            desc = propertiesArray.getString("description");

            JSONArray coordinates = geometryArray.getJSONArray("coordinates");

            latitude = coordinates.getDouble(0);
            longitude = coordinates.getDouble(1);

            Place placeObj = new Place("magasin", name, desc, (float)latitude, (float)longitude);
            outputPlaces.add(placeObj);
        }

        Place[] finalOutputArray = new Place[outputPlaces.size()];
        for (int i = 0; i < outputPlaces.size(); i++){
            finalOutputArray[i] = outputPlaces.get(i);
        }

        return finalOutputArray;
    }
}
