package com.example.ecoleapandroid.utils;

import android.content.Context;

import com.example.ecoleapandroid.R;

import java.io.IOException;
import java.io.InputStream;

public class FileUtils {
    public static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;
        try {
            //context.getResources().getIdentifier(fileName, "raw", context.getPackageName())
            InputStream is = context.getResources().openRawResource(R.raw.magasins_zero_dechet);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }
}
