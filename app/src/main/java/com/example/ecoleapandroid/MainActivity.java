package com.example.ecoleapandroid;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import com.example.ecoleapandroid.database.AdviceViewModelFactory;
import com.example.ecoleapandroid.database.PlaceViewModelFactory;
import com.example.ecoleapandroid.database.RecipeViewModelFactory;
import com.example.ecoleapandroid.database.advice.AdviceViewModel;
import com.example.ecoleapandroid.database.place.PlaceViewModel;
import com.example.ecoleapandroid.database.recipe.RecipeViewModel;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.ecoleapandroid.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;

    private static AdviceViewModel adviceViewModel;
    private static PlaceViewModel placeViewModel;
    private static RecipeViewModel recipeViewModel;
    private static Activity currentActivity;
    private SharedPreferences prefs = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        currentActivity = this;

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        placeViewModel = new ViewModelProvider(this, new PlaceViewModelFactory(this.getApplication())).get(PlaceViewModel.class);
        adviceViewModel = new ViewModelProvider(this, new AdviceViewModelFactory(this.getApplication())).get(AdviceViewModel.class);
        recipeViewModel = new ViewModelProvider(this, new RecipeViewModelFactory(this.getApplication())).get(RecipeViewModel.class);

        prefs = getSharedPreferences("com.example.ecoleapandroid", MODE_PRIVATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (prefs.getBoolean("firstRun", true)) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setMessage("Bienvenue sur l'app EcoLeap, l'app qui essaie de te faire changer tes habitudes !\n\nTu peux retrouver des conseils, localiser les magasins de vrac et zéro-déchet les plus proches puis consulter une liste de recette végétariennes voire en ajouter \uD83D\uDE04 \n\nAmuse-toi bien !");
            alertDialogBuilder.setTitle("Présentation EcoLeap");
            alertDialogBuilder.setCancelable(true);

            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            prefs.edit().putBoolean("firstRun", false).apply();
        }
    }

    public static PlaceViewModel getInstance(){
        return placeViewModel;
    }

    public static AdviceViewModel getAdviceVMInstance(){
        return adviceViewModel;
    }

    public static RecipeViewModel getRecipeVMInstance(){return recipeViewModel;}

    public static void restartActivity() {
        if (Build.VERSION.SDK_INT >= 11) {
            currentActivity.recreate();
        } else {
            currentActivity.finish();
            currentActivity.startActivity(currentActivity.getIntent());
        }
    }
}