package com.example.ecoleapandroid.database.place;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.ecoleapandroid.models.Place;

import java.util.List;

@Dao
public interface PlaceDAO {
    @Insert
    void insert(Place place);

    @Insert
    void insertAll(Place... places);

    @Query("SELECT * from place")
    List<Place> getAllPlace();
}
