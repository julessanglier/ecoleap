package com.example.ecoleapandroid.database;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.example.ecoleapandroid.MainActivity;
import com.example.ecoleapandroid.database.advice.AdviceDAO;
import com.example.ecoleapandroid.database.place.PlaceDAO;
import com.example.ecoleapandroid.database.recipe.RecipeDAO;
import com.example.ecoleapandroid.models.Advice;
import com.example.ecoleapandroid.models.Place;
import com.example.ecoleapandroid.models.Recipe;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;

import java.util.concurrent.Executors;

@Database(entities = {Advice.class, Place.class, Recipe.class}, version = 1)
public abstract class EcoleapRoomDatabase extends RoomDatabase{
    public abstract AdviceDAO adviceDAO();
    public abstract PlaceDAO placeDAO();
    public abstract RecipeDAO recipeDAO();

    private static EcoleapRoomDatabase INSTANCE;

    public synchronized static EcoleapRoomDatabase getInstance(final Context context){
        if (INSTANCE == null){
            INSTANCE = buildDatabase(context);
        }

        return INSTANCE;
    }

    private static EcoleapRoomDatabase buildDatabase(final Context context){
        return Room.databaseBuilder(context,
        EcoleapRoomDatabase.class,
        "ecoleap_db")
        .addCallback(new Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
                Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            for (Place p : Place.populateMagasins(context.getApplicationContext())){
                                getInstance(context.getApplicationContext()).placeDAO().insert(p);
                            }

                            for (Advice a : Advice.populateData()){
                                getInstance(context.getApplicationContext()).adviceDAO().insert(a);
                            }

                            for (Recipe r : Recipe.populateRecipes()){
                                getInstance(context.getApplicationContext()).recipeDAO().insert(r);
                            }

                            MainActivity.restartActivity();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }).allowMainThreadQueries()
        .build();
    }

    /*public static EcoleapRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (EcoleapRoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            EcoleapRoomDatabase.class, "ecoleap_database").build();
                }
            }
        }
        return INSTANCE;
    }*/

}
