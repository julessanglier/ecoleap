package com.example.ecoleapandroid.database.advice;

import android.app.Application;
import android.os.AsyncTask;

import com.example.ecoleapandroid.database.EcoleapRoomDatabase;
import com.example.ecoleapandroid.models.Advice;

import java.util.List;

public class AdviceRepository {
    private AdviceDAO adviceDAO;
    private List<Advice> advicesList;

    public AdviceRepository(Application application){
        EcoleapRoomDatabase db = EcoleapRoomDatabase.getInstance(application);
        adviceDAO = db.adviceDAO();
        advicesList = adviceDAO.getAdvices();
    }

    public void insert(Advice a){
        new insertAsyncTask(adviceDAO).execute(a);
    }

    private static class insertAsyncTask extends AsyncTask<Advice,Void,Void> {
        private AdviceDAO adviceDao;

        insertAsyncTask(AdviceDAO dao){adviceDao = dao;}

        @Override
        protected Void doInBackground(Advice... advice) {
            adviceDao.insert(advice[0]);
            return null;
        }
    }

    public List<Advice> getAdvices(){
        return advicesList;
    }
}
