package com.example.ecoleapandroid.database.advice;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.ecoleapandroid.models.Advice;

import java.util.List;

public class AdviceViewModel extends AndroidViewModel {
    private AdviceRepository adviceRepository;
    private List<Advice> advicesList;

    public AdviceViewModel(@NonNull Application application){
        super(application);
        adviceRepository = new AdviceRepository(application);
        advicesList = adviceRepository.getAdvices();
    }

    public List<Advice> getAdvices(){
        return advicesList;
    }

    public void insert(Advice a){
        adviceRepository.insert(a);
    }
}
