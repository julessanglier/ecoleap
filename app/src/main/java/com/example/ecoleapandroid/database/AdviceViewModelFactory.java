package com.example.ecoleapandroid.database;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.ecoleapandroid.database.advice.AdviceViewModel;
import com.example.ecoleapandroid.database.place.PlaceViewModel;

public class AdviceViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;

    public AdviceViewModelFactory(Application application) {
        mApplication = application;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new AdviceViewModel(mApplication);
    }
}