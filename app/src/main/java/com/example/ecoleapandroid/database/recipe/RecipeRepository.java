package com.example.ecoleapandroid.database.recipe;

import android.app.Application;
import android.os.AsyncTask;

import com.example.ecoleapandroid.database.EcoleapRoomDatabase;
import com.example.ecoleapandroid.models.Recipe;

import java.util.List;

public class RecipeRepository {
    private RecipeDAO recipeDAO;
    private List<Recipe> recipesList;

    public RecipeRepository(Application application){
        EcoleapRoomDatabase db = EcoleapRoomDatabase.getInstance(application);
        recipeDAO = db.recipeDAO();
        recipesList = recipeDAO.getRecipes();
    }

    public void insert(Recipe r){
        new RecipeRepository.insertAsyncTask(recipeDAO).execute(r);
    }

    private static class insertAsyncTask extends AsyncTask<Recipe,Void,Void> {
        private RecipeDAO recipeDAO;

        insertAsyncTask(RecipeDAO dao){recipeDAO = dao;}

        @Override
        protected Void doInBackground(Recipe... recipe) {
            recipeDAO.insert(recipe[0]);
            return null;
        }
    }

    public List<Recipe> getRecipes(){
        return recipesList;
    }
}
