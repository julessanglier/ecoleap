package com.example.ecoleapandroid.database.advice;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.ecoleapandroid.models.Advice;

import java.util.List;

@Dao
public interface AdviceDAO {
    @Insert
    void insert(Advice advice);

    @Query("SELECT * FROM advice")
    List<Advice> getAdvices();
}
