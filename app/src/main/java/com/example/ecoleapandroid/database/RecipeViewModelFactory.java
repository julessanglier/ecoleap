package com.example.ecoleapandroid.database;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.ecoleapandroid.database.recipe.RecipeViewModel;

public class RecipeViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;

    public RecipeViewModelFactory(Application application) {
        mApplication = application;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new RecipeViewModel(mApplication);
    }
}
