package com.example.ecoleapandroid.database.place;

import android.app.Application;

import com.example.ecoleapandroid.database.EcoleapRoomDatabase;
import com.example.ecoleapandroid.models.Place;

import java.util.List;

public class PlaceRepository {
    private PlaceDAO placeDAO;
    private List<Place> allPlaces;

    public PlaceRepository(Application application){
        EcoleapRoomDatabase db = EcoleapRoomDatabase.getInstance(application);
        placeDAO = db.placeDAO();
        allPlaces = placeDAO.getAllPlace();
    }

    public List<Place> getAllPlace() {
        return allPlaces;
    }
}