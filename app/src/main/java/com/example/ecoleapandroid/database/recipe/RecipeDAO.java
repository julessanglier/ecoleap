package com.example.ecoleapandroid.database.recipe;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.ecoleapandroid.models.Recipe;

import java.util.List;

@Dao
public interface RecipeDAO {
    @Insert
    void insert(Recipe recipe);

    @Query("SELECT * FROM recipe")
    List<Recipe>getRecipes();
}
