package com.example.ecoleapandroid.database.recipe;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.ecoleapandroid.database.advice.AdviceRepository;
import com.example.ecoleapandroid.models.Advice;
import com.example.ecoleapandroid.models.Recipe;

import java.util.List;

public class RecipeViewModel extends AndroidViewModel {
    private RecipeRepository recipeRepository;
    private List<Recipe> recipesList;

    public RecipeViewModel(@NonNull Application application){
        super(application);
        recipeRepository = new RecipeRepository(application);
        recipesList = recipeRepository.getRecipes();
    }

    public List<Recipe> getRecipes(){
        return recipesList;
    }

    public void insert(Recipe r){
        recipeRepository.insert(r);
    }
}
