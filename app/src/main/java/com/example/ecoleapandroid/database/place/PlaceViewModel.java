package com.example.ecoleapandroid.database.place;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.example.ecoleapandroid.models.Place;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PlaceViewModel extends AndroidViewModel {
    private PlaceRepository placeRepository;
    private List<Place> allPlaces;

    public PlaceViewModel(Application application) {
        super(application);
        placeRepository = new PlaceRepository(application);
        allPlaces = placeRepository.getAllPlace();
    }

    public List<Place> getAllPlace() {
        return allPlaces;
    }

}
