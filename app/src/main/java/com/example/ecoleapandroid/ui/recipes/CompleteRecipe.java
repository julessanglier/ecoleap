package com.example.ecoleapandroid.ui.recipes;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ecoleapandroid.R;

public class CompleteRecipe extends AppCompatActivity {

    private TextView recipeTitle;
    private TextView recipeIngredient;
    private TextView recipeText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_recipe);
        recipeTitle = findViewById(R.id.title_place);
        recipeIngredient = findViewById(R.id.ingredient_place);
        recipeText = findViewById(R.id.recipe_place);
        Bundle extras = getIntent().getExtras();
        if (extras != null ){

            String recipeTitleText = extras.getString("title");
            String recipeIngredientText = extras.getString("ingredient");
            String recipeTextText = extras.getString("text");

            recipeTitle.setText(recipeTitleText);
            recipeIngredient.setText(recipeIngredientText);
            recipeText.setText(recipeTextText);
        }
    }
}
