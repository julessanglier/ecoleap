package com.example.ecoleapandroid.ui.advices;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecoleapandroid.MainActivity;
import com.example.ecoleapandroid.R;
import com.example.ecoleapandroid.databinding.FragmentAdvicesBinding;
import com.example.ecoleapandroid.models.Advice;
import com.example.ecoleapandroid.tools.advice.AdviceAdapter;

import java.util.ArrayList;
import java.util.List;

public class AdvicesFragment extends Fragment {
    private FragmentAdvicesBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private AdviceAdapter adviceAdapter;
    private List<Advice> advices = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentAdvicesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        advices = MainActivity.getAdviceVMInstance().getAdvices();
        Log.d("ADVICES", String.valueOf(advices.size()));

        RecyclerView recyclerView = root.findViewById(R.id.recyclerViewAdvice);
        linearLayoutManager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        adviceAdapter = new AdviceAdapter(advices);
        recyclerView.setAdapter(adviceAdapter);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}