package com.example.ecoleapandroid.ui.recipes;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ecoleapandroid.MainActivity;
import com.example.ecoleapandroid.R;
import com.example.ecoleapandroid.databinding.FragmentRecipesBinding;
import com.example.ecoleapandroid.databinding.FragmentRecipesBinding;
import com.example.ecoleapandroid.models.Recipe;
import com.example.ecoleapandroid.tools.recipe.RecipeAdapter;

import java.util.ArrayList;
import java.util.List;

public class RecipesFragment extends Fragment {
    private FragmentRecipesBinding binding;
    private LinearLayoutManager linearLayoutManager;
    private RecipeAdapter recipeAdapter;
    private List<Recipe> recipes = new ArrayList<>();

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentRecipesBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        recipes = MainActivity.getRecipeVMInstance().getRecipes();
        Log.d("RECIPES", String.valueOf(recipes.size()));

        RecyclerView recyclerView = root.findViewById(R.id.recyclerViewRecipe);
        linearLayoutManager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        recipeAdapter = new RecipeAdapter(recipes);
        recyclerView.setAdapter(recipeAdapter);

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
