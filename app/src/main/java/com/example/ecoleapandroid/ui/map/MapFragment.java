package com.example.ecoleapandroid.ui.map;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.ecoleapandroid.MainActivity;
import com.example.ecoleapandroid.R;
import com.example.ecoleapandroid.databinding.FragmentMapBinding;
import com.example.ecoleapandroid.models.Place;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static androidx.core.content.ContextCompat.getSystemService;

public class MapFragment extends Fragment implements OnMapReadyCallback, LocationListener {

    private FragmentMapBinding binding;
    private MapView mapView;
    private LocationManager locationManager;
    private double userLatitude;
    private double userLongitude;
    private double radius;
    private int placesFound;
    private Circle mapCircle;
    private GoogleMap map;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        binding = FragmentMapBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        radius = 5000;
        placesFound = 0;

        if (ActivityCompat.checkSelfPermission(getContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getContext(),
                        android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION,
                            android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }

        Button locateButton = (Button) root.findViewById(R.id.buttonLocalisation);
        locateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               getLocation();
            }
        });

        Button changeRadius = (Button)root.findViewById(R.id.buttonChangeRadius);
        changeRadius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText radiusTv = (EditText)root.findViewById(R.id.editTextNumberRadius);
                getMapFragment().mapCircle.remove();
                getMapFragment().radius = Double.parseDouble(radiusTv.getText().toString());
                getLocation(); //pour appeler onLocationChanged
            }
        });

        mapView = (MapView)root.findViewById(R.id.mapView2);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
        mapView.setClickable(true);

        getLocation();

        return root;
    }

    private MapFragment getMapFragment(){
        return this;
    }

    private void getLocation() {
        try {
            locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,5000,5,this);
            Toast.makeText(this.getContext(), "Localisation automatique en cours ...", Toast.LENGTH_LONG).show();
        }catch (SecurityException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        userLatitude = location.getLatitude();
        userLongitude = location.getLongitude();

        map.moveCamera(CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(new LatLng(userLatitude, userLongitude), 12.0f)));
        this.mapCircle = map.addCircle(new CircleOptions()
                .center(new LatLng(userLatitude, userLongitude))
                .radius(radius)
                .fillColor(Color.argb(50, 95, 163, 53)));

        placesFound = 0;
        VisibleRegion vr = map.getProjection().getVisibleRegion();
        for (Place p : MainActivity.getInstance().getAllPlace()){
            LatLng loc = new LatLng(p.getLongitude(), p.getLatitude());

            if (vr.latLngBounds.contains(loc)){
                placesFound++;
            }
        }

        TextView nbLieuxTrouves = (TextView)getView().findViewById(R.id.textViewLieuxTrouves);
        if (placesFound > 1){
            nbLieuxTrouves.setText(placesFound + " lieux trouvés !");
        }else{
            nbLieuxTrouves.setText(placesFound + " lieu trouvé !");
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {

    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        Toast.makeText(getContext(), "Please Enable GPS and Internet", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        map = googleMap;
        map.getUiSettings().setMyLocationButtonEnabled(false);
        if (ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this.getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), "Erreur de permission", Toast.LENGTH_LONG).show();
            return;
        }

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker arg0) {
                Place p = (Place)arg0.getTag();

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getMapFragment().getContext());
                alertDialogBuilder.setMessage(p.getDescription());
                alertDialogBuilder.setTitle(p.getName());
                alertDialogBuilder.setCancelable(true);

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });

        map.setMyLocationEnabled(true);
        map.setTrafficEnabled(true);

        for (Place p : MainActivity.getInstance().getAllPlace()){
            LatLng loc = new LatLng(p.getLongitude(), p.getLatitude());
            map.addMarker(new MarkerOptions()
                    .position(loc)
                    .title(p.getName())).setTag(p);
        }
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}